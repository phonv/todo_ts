import React from 'react'
import { Container, Heading } from '@chakra-ui/react'

export const Header: React.FC = () => {
    return (
        <Container>
            <Heading>TS TO DO APP</Heading>
        </Container>
    )
}
