import React, { useRef, useState } from 'react'
import {
    Text,
    Wrap,
    Button,
    Drawer,
    DrawerBody,
    DrawerFooter,
    DrawerHeader,
    DrawerOverlay,
    DrawerContent,
    DrawerCloseButton,
    useDisclosure,
    Input,
    useToast,
} from '@chakra-ui/react'

import { ListTask } from '../molecules/ListTask'

import { PatchTask, Task } from '../../../types/HomePage'

import { createTask, deleteTask, updateTask } from '../../../services/task'

type Props = {
    list: Task[]
    onRefresh: () => Promise<void>
}

export const TodoList: React.FC<Props> = (props) => {
    const { list, onRefresh } = props
    const { isOpen, onOpen, onClose } = useDisclosure()
    const toast = useToast()
    const [newTask, setNewTask] = useState<string>('')
    const btnRef = useRef(null)

    const onUpdate = async (item: PatchTask): Promise<void> => {
        await updateTask(item)
        toast({
            title: 'Update successfully',
            status: 'success',
            duration: 9000,
            isClosable: true,
        })
        return onRefresh()
    }

    const onDelete = async (id: string): Promise<void> => {
        await deleteTask(id)
        toast({
            title: 'Delete successfully',
            status: 'success',
            duration: 9000,
            isClosable: true,
        })
        return onRefresh()
    }

    const onSaveNewTask = async (): Promise<void> => {
        if (!newTask) {
            toast({
                title: 'Please fill require field',
                status: 'info',
                duration: 3000,
                isClosable: true,
            })
            return
        }
        await createTask({ name: newTask, isCompleted: false })
        setNewTask('')
        toast({
            title: 'A new task created.',
            status: 'success',
            duration: 9000,
            isClosable: true,
        })
        onClose()
        return onRefresh()
    }

    return (
        <Wrap direction="column" alignItems="baseline">
            <Wrap direction="row" alignItems="center" justify="space-between">
                <Text fontSize="3xl">Todo List</Text>
                <Button
                    colorScheme="blue"
                    size="sm"
                    variant="outline"
                    onClick={onOpen}
                >
                    New
                </Button>
            </Wrap>
            <ListTask onEdit={onUpdate} onDelete={onDelete} list={list} />
            <Drawer
                isOpen={isOpen}
                placement="right"
                onClose={onClose}
                finalFocusRef={btnRef}
                closeOnEsc={true}
            >
                <DrawerOverlay />
                <DrawerContent>
                    <DrawerCloseButton />
                    <DrawerHeader>Create new task</DrawerHeader>

                    <DrawerBody>
                        <Input
                            value={newTask}
                            onChange={(e) => {
                                setNewTask(e.target.value)
                            }}
                        />
                    </DrawerBody>

                    <DrawerFooter>
                        <Button variant="outline" mr={3} onClick={onClose}>
                            Cancel
                        </Button>
                        <Button colorScheme="blue" onClick={onSaveNewTask}>
                            Save
                        </Button>
                    </DrawerFooter>
                </DrawerContent>
            </Drawer>
        </Wrap>
    )
}
