import React from 'react'
import { Wrap, Text } from '@chakra-ui/react'

import { ListTask } from '../molecules/ListTask'

import { Task } from '../../../types/HomePage'

type Props = {
    list: Task[]
}

export const CompletedList: React.FC<Props> = (props) => {
    const { list } = props
    return (
        <Wrap direction="column" alignItems="left">
            <Text fontSize="3xl">Completed List</Text>
            <ListTask list={list} />
        </Wrap>
    )
}
