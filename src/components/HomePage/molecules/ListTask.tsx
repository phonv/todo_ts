import React, { useEffect, useState } from 'react'
import {
    List,
    ListItem,
    ListIcon,
    Editable,
    EditableInput,
    EditablePreview,
    Text,
    Wrap,
    IconButton,
    Box,
} from '@chakra-ui/react'
import { CheckIcon, DeleteIcon } from '@chakra-ui/icons'
import { MdCheckCircle } from 'react-icons/md'

import { PatchTask, PostTask, Task } from '../../../types/HomePage'

type Props = {
    list: Task[]
    onCreate?: (payload: PostTask) => Promise<Task>
    onEdit?: (payload: PatchTask) => Promise<void>
    onDelete?: (id: string) => void
}

export const ListTask: React.FC<Props> = (props) => {
    const { list, onEdit, onDelete } = props

    const [tasks, setTasks] = useState<Task[]>([])

    useEffect(() => {
        setTasks(list)
    }, [list])

    const onChange = (id: string, val: string): Promise<void> | void => {
        const raw = [...tasks]
        const task = raw.find((item) => item.id === id)
        if (!task || !onEdit || task.name === val) {
            return
        }
        return onEdit({ ...task, name: val })
    }

    const onCompleteTask = (id: string): Promise<void> | void => {
        const raw = [...tasks]
        const task = raw.find((item) => item.id === id)
        if (!task || !onEdit || task.isCompleted) {
            return
        }
        return onEdit({ ...task, isCompleted: true })
    }

    const onDeleteTask = (id: string): void => {
        if (!id || !onDelete) {
            return
        }
        return onDelete(id)
    }

    return (
        <List spacing={3} textAlign="initial">
            {tasks.map((item) => (
                <ListItem
                    display="flex"
                    alignItems="center"
                    key={Math.random()}
                >
                    {item.isCompleted ? (
                        <>
                            <ListIcon as={MdCheckCircle} color="green.500" />
                            <Text>{item.name}</Text>
                        </>
                    ) : (
                        <Wrap
                            width="100%"
                            justify="space-between"
                            alignContent="center"
                            p={2}
                            borderWidth="1px"
                            borderRadius="lg"
                            fontSize="md"
                        >
                            <Editable
                                defaultValue={item.name}
                                onSubmit={(e) => onChange(item.id, e)}
                            >
                                <EditablePreview />
                                <EditableInput />
                            </Editable>
                            <Box>
                                <IconButton
                                    mr={2}
                                    size="sm"
                                    colorScheme="teal"
                                    aria-label="Edit"
                                    icon={<CheckIcon />}
                                    onClick={() => onCompleteTask(item.id)}
                                />
                                <IconButton
                                    size="sm"
                                    colorScheme="red"
                                    aria-label="Delete"
                                    icon={<DeleteIcon />}
                                    onClick={() => onDeleteTask(item.id)}
                                />
                            </Box>
                        </Wrap>
                    )}
                </ListItem>
            ))}
        </List>
    )
}
