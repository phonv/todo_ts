import * as React from 'react'
import { Box, ChakraProvider, Grid, theme } from '@chakra-ui/react'
import { HomePage } from './containers/HomePage'
import { ColorModeSwitcher } from './ColorModeSwitcher'

export const App: React.FC = () => (
    <ChakraProvider theme={theme}>
        <Box textAlign="center" fontSize="xl" fontFamily={'monospace'}>
            <Grid minH="100vh" alignContent="start" p={3}>
                <ColorModeSwitcher justifySelf="flex-end" />
                <HomePage />
            </Grid>
        </Box>
    </ChakraProvider>
)
