import { AxiosResponse } from 'axios'

import axios from '../axios'

import { PatchTask, PostTask } from '../types/HomePage'

export const getListTask = (): Promise<AxiosResponse> => {
    return axios.get('tasks')
}

export const createTask = (payload: PostTask): Promise<AxiosResponse> => {
    return axios.post(`tasks`, payload)
}

export const updateTask = (payload: PatchTask): Promise<AxiosResponse> => {
    return axios.put(`tasks/${payload.id}`, payload)
}

export const deleteTask = (id: string): Promise<AxiosResponse> => {
    return axios.delete(`tasks/${id}`)
}
