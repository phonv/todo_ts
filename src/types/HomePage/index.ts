export type Task = {
    id: string
    name: string
    isCompleted: boolean
}

export type PostTask = Omit<Task, 'id'>

export type PatchTask = Partial<PostTask> & { id: string }
