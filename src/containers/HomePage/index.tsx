import React, { useEffect, useState } from 'react'
import { Box, Grid } from '@chakra-ui/react'

import { Header } from '../../components/HomePage/organisms/Header'
import { TodoList } from '../../components/HomePage/organisms/TodoList'
import { CompletedList } from '../../components/HomePage/organisms/CompletedList'

import { Task } from '../../types/HomePage'

import { getListTask } from '../../services/task'

export const HomePage: React.FC = () => {
    const [completedList, setCompletedList] = useState<Task[]>([])
    const [todoList, setTodoList] = useState<Task[]>([])

    const fetchTasks = async (): Promise<void> => {
        const { data } = await getListTask()

        if (data) {
            setTodoList(data.filter((item: Task) => !item.isCompleted))
            setCompletedList(data.filter((item: Task) => item.isCompleted))
        }
    }

    useEffect(() => {
        fetchTasks()
    }, [])

    return (
        <div>
            <Header />
            <Grid templateColumns="repeat(2, 1fr)" mt="5" padding={'10'}>
                <Box mr={5} borderWidth={1} borderRadius={4} p={5} h={'70vh'}>
                    <TodoList onRefresh={fetchTasks} list={todoList} />
                </Box>
                <Box ml={5} borderWidth={1} p={5} borderRadius={'4px'}>
                    <CompletedList list={completedList} />
                </Box>
            </Grid>
        </div>
    )
}
